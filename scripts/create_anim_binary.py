import numpy as np
import cv2 as cv
import sys
import os
import functools

def slice_per(source, step):     
    return [source[i::step] for i in range(step)]

def drop_diff(current_img, previous_img):
    diff_table=bytearray()
    for idx, value in enumerate(current_img):
        if current_img[idx] != previous_img[idx]:
            diff_table.extend(idx.to_bytes(2, 'big'))
            diff_table.append(value)
    return diff_table

def convert_img_to_vcsimg(img):
    newimg = np.copy(img)
    vcsimg = bytearray()
    for rows in newimg:
        for eight_pixels in slice_per(rows, 12):
            pixel_byte = 0
            for idx, value in enumerate(map(lambda x: (int(x[0])+int(x[1])+int(x[2]))>3, eight_pixels)):
                if value:
                    pixel_byte |= 1 << (7-idx)
            vcsimg.append(pixel_byte)
    return vcsimg

if len(sys.argv) <= 1: 
    print('Usage: python create_anim_binary.py <directory>')
    exit(0)

dirpath = sys.argv[1]
(_, _, filenames) = next(os.walk(dirpath), (None, None, []))

animation_array = bytearray()
prev_img = None
for file in filenames:
    img = cv.imread(os.path.join(dirpath, file))
    img = convert_img_to_vcsimg(img)
    if prev_img is None:
        animation_array.extend(img)
    else:
        animation_array.extend(drop_diff(img, prev_img))
    prev_img = img

print(len(animation_array))
newfile = open('temp.bin', 'wb')
newfile.write(animation_array)
newfile.close()



